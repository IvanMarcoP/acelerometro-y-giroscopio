using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Controller : MonoBehaviour
{

    private Rigidbody2D rb;

    public float speed = 10f;
    public float rotationSpeed = 10f;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 tilt = Input.acceleration;

        Vector2 move = new Vector2(tilt.x, tilt.y) * speed;

        rb.velocity = move;

        Quaternion rotation = Input.gyro.attitude;
        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, rotationSpeed * Time.deltaTime);
    }
}
